import argparse
from apiclient.discovery import build
from oauth2client.client import SignedJwtAssertionCredentials
import httplib2
from oauth2client import file


class GoogleAnalyticsAPI:


  def __init__(self, api_name, api_version, scope, key_file_location,
                  service_account_email):
    self.service = self.set_service(api_name, api_version, scope, key_file_location,
                  service_account_email)
    self.account = None
    self.property = None
    self.profile = None

    self.possible_metrics, self.possible_dimensions = self.get_columns()

    self.columns = {'DIMENSION': [], 'METRIC': []}


  def set_service(self, api_name, api_version, scope, key_file_location,
                  service_account_email):

    f = open(key_file_location, 'rb')
    key = f.read()
    f.close()

    credentials = SignedJwtAssertionCredentials(service_account_email, key,
      scope=scope)
    http = credentials.authorize(httplib2.Http())
    service = build(api_name, api_version, http=http)

    return service


  def set_account(self, account_arg):
    accounts = self.service.management().accounts().list().execute()
    if accounts.get('items'):
      account_gen = (a for a in accounts.get('items') if a['name'] == account_arg)
      self.account = next(account_gen, None)


  def set_property(self, property_arg):
    properties = self.service.management().webproperties().list(
        accountId=self.account.get('id')).execute()

    if properties.get('items'):
      property_gen = (p for p in properties.get('items') if p['name'] == property_arg)
      self.property = next(property_gen, None)


  def set_profile(self, profile_arg):
    profiles = self.service.management().profiles().list(
        accountId=self.account.get('id'),
        webPropertyId=self.property.get('id')).execute()

    if profiles.get('items'):
      profile_gen = (pf for pf in profiles.get('items') if pf['name'] == profile_arg)
      self.profile = next(profile_gen, None)
      

  def get_column_objects(self):
    columns_data = self.service.metadata().columns().list(reportType='ga').execute()
    return columns_data


  def get_columns(self):
    columns_data = self.get_column_objects()
    metrics = set()
    dimensions = set()
    for column in columns_data.get('items', []):
      attributes = column.get('attributes')
      ctype = attributes.get('type')
      status = attributes.get('status')
      if ctype == 'METRIC' and status == 'PUBLIC':
        metrics.add(column.get('id'))
      elif ctype == 'DIMENSION' and status == 'PUBLIC':
        dimensions.add(column.get('id'))
    return metrics, dimensions


  def check_column_exists(self, column, column_type):
    if column_type == 'DIMENSION':
      return column in self.possible_dimensions
    elif column_type == 'METRIC':
      return column in self.possible_metrics


  def set_columns(self, columns, column_type):
    for column in columns:
      try:
        self.check_column_exists(column, column_type)
        self.columns[column_type].append(column)
      except:
        raise Exception(column + ' is not a valid ' + column_type.lower())


  def get_results(self, start_date, end_date, metrics, dimensions):
    return self.service.data().ga().get(
        ids='ga:' + self.profile.get('id'),
        start_date=start_date,
        end_date=end_date,
        metrics=metrics,
        dimensions=dimensions).execute()

