from GoogleAnalyticsAPI import GoogleAnalyticsAPI


def main(start_date, end_date):
  scope = ['https://www.googleapis.com/auth/analytics.readonly']
  service_account_email = '1040782786939-lvr2fnmqtm69rq4o8505ocvnb776f5r0@developer.gserviceaccount.com'
  key_file_location = './client_secrets.p12'

  gapi = GoogleAnalyticsAPI('analytics', 'v3', scope, key_file_location,
    service_account_email)

  gapi.set_account('Observe to Exist')
  gapi.set_property('Observe to Exist')
  gapi.set_profile('Observe to Exist')

  metrics = str(raw_input('Metrics: '))
  dimensions = str(raw_input('Dimensions: '))

  gapi.set_columns(metrics, 'METRIC')
  gapi.set_columns(dimensions, 'DIMENSION')

  results = gapi.get_results(start_date, end_date, metrics, dimensions)

  return results


if __name__ == '__main__':
  import sys

  start_date = '7daysAgo'
  end_date = 'today'

  try:
    start_date = sys.argv[1]
    end_date = sys.argv[2]

  except:
    pass

  data = main(start_date, end_date)
  headers = [header['name'] for header in data['columnHeaders']]
  rows = data['rows']
  print headers
  for row in rows:
    print row

